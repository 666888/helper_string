<?php

/**
 * 文本处理库
 */

if (!function_exists('filteRepeatStr')) {
	/**
	 * [过滤重复的子文本]
	 *
	 * @Author   chaonan
	 *
	 * @DateTime 2022-01-23T15:27:09+0800
	 *
	 * @param    [string]                   $str    [文本内容]
	 * @param    [string]                   $substr [子文本内容]
	 *
	 * @return   [string]                           [返回处理后的文本内容]
	 */
	function filteRepeatStr($str, $substr) {
		//将特殊字符转义 避免正则表达式异常
		$preg_substr = str_replace(["[", "]", ".", "?", "+", "^", "*"], ["\\[", "\\]", "\\.", "\\?", "\\+", "\\^", "\\*"], $substr);
		$base64 = base64_encode($substr);
		$str = preg_replace("/{$preg_substr}/i", $base64, $str, 1);
		$str = str_replace($substr, "", $str);
		$str = str_replace($base64, $substr, $str);
		return $str;
	}

}
